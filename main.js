import Vue from 'vue'
import App from './App'
import HttpModel from './utils/api.js'

Vue.prototype.$http = new HttpModel();
// ---------------------------------------------
import VConsole from 'vconsole';
var vConsole = new VConsole();
// ---------------------------------------------

// 关闭打印输出
// console.log = () => {}	

// 引入uview组件
import uView from 'uview-ui';
Vue.use(uView);

let vuexStore = require("@/store/$u.mixin.js");
Vue.mixin(vuexStore);
import store from '@/store';

Vue.config.productionTip = false

App.mpType = 'app'

const app = new Vue({
	store,
	...App
})

import httpApi from '@/common/http.api.js'
Vue.use(httpApi, app)

// http拦截器，此为需要加入的内容，如果不是写在common目录，请自行修改引入路径
import httpInterceptor from '@/common/http.interceptor.js'
// 这里需要写在最后，是为了等Vue创建对象完成，引入"app"对象(也即页面的"this"实例)
Vue.use(httpInterceptor, app)


// 自定义工具
import utils from '@/common/utils.js'
Vue.use(utils, app)

app.$mount()
console.log('app 挂在完成')
// 登录检查
let url = window.location.href 
console.log('main,js   url========',url)
console.log('main,js   this========',this)
