import Vue from 'vue'
import Vuex from 'vuex'
// 变量持久化
Vue.use(Vuex)

let lifeData = {}

try {
  // 尝试获取本地是否存在lifeData变量，第一次启动APP时是不存在的
  lifeData = uni.getStorageSync('lifeData')
} catch (e) {}

// 在state中声明的变量，如果需要永久存储，需要将名字加到下面的数组中
let saveStateKeys = ['vuex_request_token', 'vuex_refresh_token', 'vuex_user_info', 'vuex_location', 'vuex_city_list', 'ENV']

// 保存变量到本地存储中
const saveLifeData = function (key, value) {
  // 判断要保存得到变量是否存在
  if (saveStateKeys.indexOf(key) != -1) {
    // 获取lifeData对象
    let tmp = uni.getStorageSync('lifeData')
    // 第一次打开APP，不存在lifeData变量，设置为{}空对象
    tmp = tmp ? tmp : {}

    // 将数据保存到对象中
    tmp[key] = value

    // 保存lifedata
    uni.setStorageSync('lifeData', tmp)
  }
}
const store = new Vuex.Store({
  state: {
    // 相当于是全局变量 ，保存在内存中，程序重启后就没有了，
    // 其他任何地方要使用此变量，必须现在下面做声明

    // 加上vuex_前缀，是防止变量名冲突，也让人一目了然
    vuex_request_token: lifeData.vuex_request_token ? lifeData.vuex_request_token : 'no_value',
    vuex_refresh_token: lifeData.vuex_refresh_token ? lifeData.vuex_refresh_token : 'no_value',
    vuex_location: lifeData.vuex_location ? lifeData.vuex_location : 'no_value',
    // 记录回调地址
    vuex_back_url: '',
    // 如果vuex_version无需保存到本地永久存储，无需lifeData.vuex_version方式
    // vuex_version: '1.0.1',
    vuex_user_info: lifeData.vuex_user_info ? lifeData.vuex_user_info : 'no_value',
    vuex_from_uid: '',
    vuex_sysinfo: '',
    //  全局永久存储城市列表
    vuex_city_list: lifeData.vuex_city_list ? lifeData.vuex_city_list : 'no_value',
    // 环境变量 ==0 测试环境； =1 正式环境
    ENV: 0,
    // 测试线
    vuex_api_HEADER: '',
    vuex_index_page: ''
  },
  mutations: {
    $uStore(state, payload) {
      // 判断是否多层级调用，state中为对象存在的情况，诸如user.info.score = 1
      let nameArr = payload.name.split('.')
      let saveKey = ''
      let len = nameArr.length
      if (nameArr.length >= 2) {
        let obj = state[nameArr[0]]
        for (let i = 1; i < len - 1; i++) {
          obj = obj[nameArr[i]]
        }
        obj[nameArr[len - 1]] = payload.value
        saveKey = nameArr[0]
      } else {
        // 单层级变量，在state就是一个普通变量的情况
        state[payload.name] = payload.value
        saveKey = payload.name
      }
      // 保存变量到本地，见顶部函数定义
      saveLifeData(saveKey, state[saveKey])
    }
  }
})

export default store
