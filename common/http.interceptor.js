// /common/http.interceptor.js
import md5 from "../common/md5.js";

// 这里的vm，就是我们在vue文件里面的this，所以我们能在这里获取vuex的变量，比如存放在里面的token变量
const install = (Vue, vm) => {
  // 此为自定义配置参数，具体参数见上方说明
  vm.$u.vuex('vuex_api_HEADER', vm.ENV == 0 ? 'https://xapi.rrxun.cn' : 'https://rapi.rrxun.cn')
  vm.$u.vuex('vuex_index_page', vm.ENV == 0 ? 'https://x.rrxun.cn' : 'https://web.rrxun.cn')
  Vue.prototype.$u.http.setConfig({
    baseUrl: vm.vuex_api_HEADER,
    loadingText: '努力加载中~',
    loadingTime: 800
    // ......
  })
	

  // 请求拦截，配置Token等参数
  Vue.prototype.$u.http.interceptor.request = (config) => {
    config.header.Authorization = vm.vuex_request_token
		
		if(vm.vuex_request_token !== 'no_value') {
			  let tokenStr = vm.vuex_request_token.split('.')[2]
				config.data.timestamp = new Date().getTime();
				var newkeyArr = Object.keys(config.data).sort();//升序
		    //先用Object内置的keys()方法获取待排序对象的属性名，再利用Array中的sort方法对获取的属性名进行排序，newkeyArr是一个数组
				var newObj = '';//创建一个新的对象，用于存放排好序的键值对
				for (var i = 0, len = newkeyArr.length; i < len; i++) {//遍历newkeyArr数组
						newObj = newObj + newkeyArr[i] + config.data[newkeyArr[i]];
				}
				let sign = md5.hex_md5(newObj+tokenStr)
				config.data.sign = sign;
		}
		
		if(vm.vuex_request_token === 'no_value'&& config.url!=='/index/wechatLogin'){
			// 获取登录链接的请求 不拦截；其他请求一律拦截
			return false
		}
		
		// // 登录检查
		// (async ()=>{
		// 	let url = window.location.href 
		// 	await vm.$u.utils.login_check(url)
		// })()
		
    let url_arr = ['/members/getPersonDetail', '/members/getWechatInfo', '/members/getLostPersonList']
    console.log('检查vm.vuex_from_uid=', vm.vuex_from_uid)
		
    if (vm.vuex_from_uid != '' && url_arr.includes(config.url)) {
      // 将from_uid拼接到头部 再发送
      config.url = config.url + vm.vuex_from_uid
      console.log('配置后的url=', config.url)
      vm.$u.vuex('vuex_from_uid', '')
    }
    // 获取系统信息
    if (vm.vuex_sysinfo == '') {
      uni.getSystemInfo({
        success: (res) => {
          vm.$u.vuex('vuex_sysinfo', res.system)
        }
      })
    }
  }

  // 响应拦截，判断状态码是否通过
  Vue.prototype.$u.http.interceptor.response = (res) => {
		
    // console.log('接收拦截器-→:',res)
    if (res.code >= 1000 || res.code < 0) {
      switch (res.code) {
        case 1000:
          // token过期的处理
					let url = window.location.href;
					console.log('相应拦截出获得的url', url)
          vm.$u.utils.deal_token_timeout(url)
          break

        case 1001:
          vm.$u.utils.user_info_lost()
          break

        case 1002:
          vm.$u.utils.update_user_locatin()
          break
      }
    }
    return res
  }
}


export default {
  install
}
