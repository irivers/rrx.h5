const install = (Vue, vm) => {
	vm.$u.api = {}
	/*+------------------------------------------------------------------+
	  |         登录管理 3api
	--+------------------------------------------------------------------+*/
	/* 获取登录链接*/
	vm.$u.api.get_wechat_login_link = params => vm.$u.post('/index/wechatLogin', params)

	/* 使用微信提供的code，从自己服务器拿request_token refresh_token*/
	vm.$u.api.use_wechatcode_exchange_token = (params) => vm.$u.get('/index/wechatLogin',params)

	/* 用户的token过期了 重新向服务器获取token
			将请求头的request token 改成refresh token
	*/
	vm.$u.api.refresh_user_token = () => vm.$u.post('/index/getJwt')
	/*+------------------------------------------------------------------+
	  |         会员管理 3api
	--+------------------------------------------------------------------+*/
	/* 更新实时位置*/
	vm.$u.api.update_euser_realtime_location = params => vm.$u.post('/members/updateUserPlace', params)

	/* 获取用户社交信息*/
	vm.$u.api.get_user_social_info = params => vm.$u.post('/members/getWechatInfo', params)

	/* 获取用户的分享海报*/
	vm.$u.api.get_user_QRcode_img = params => vm.$u.post('/personal/shareImg', params)
	
	/* 获取公益卡信息*/
	vm.$u.api.get_lovecard_info = () => vm.$u.post('/members/getCard')
	
	/* 更新公益卡信息*/
	vm.$u.api.update_lovecard_info = params => vm.$u.post('/members/setCard', params)
	
	/* 签到领取爱电*/
	vm.$u.api.everyday_signin = () => vm.$u.post('/members/getFilm')
	
	/* 获取会员等级信息*/
	vm.$u.api.get_user_level = params => vm.$u.post('/members/getWechatInfo', params)	
	
	/* 获取认证信息*/
	vm.$u.api.get_authinfo = () => vm.$u.post('/members/getSgs')
	
	/* 更新认证信息*/
	vm.$u.api.update_authinfo = params => vm.$u.post('/members/setSgs', params)
	
	/* 获取资产变动情况*/
	vm.$u.api.get_assets_info = params => vm.$u.post('/Members/getAsset', params)
	
	/* 获取互动列表*/
	vm.$u.api.get_it_list = params => vm.$u.post('/members/interactList', params)
	
	/* 获取互动列表2级评论*/
	vm.$u.api.get_it_Details = params => vm.$u.post('/members/interactListDetails', params)
	
	/* 获取我的分享信息*/
	vm.$u.api.get_my_share_list = params => vm.$u.post('/members/getShareList', params)

	/* 获取帮助文档*/
	vm.$u.api.get_help_doc = params => vm.$u.post('/index/helps', params)
	
	/* 获取反馈类型*/
	vm.$u.api.get_feedback_type = params => vm.$u.post('/personal/getIssue', params)
	
	/* 提交反馈类型*/
	vm.$u.api.commit_feedback = params => vm.$u.post('/personal/setIssue', params)
	
	/* 获取系统通知*/
	vm.$u.api.get_sys_note = params => vm.$u.post('/personal/getInfrom', params)
	
	/* 获取认证详情*/
	vm.$u.api.get_auth_detial = () => vm.$u.post('/members/getSgs')
	
	/* 获取会员资产信息*/
	vm.$u.api.get_asset = () => vm.$u.post('/Members/getAsset')
	
	/* 获取通系统知详情*/
	vm.$u.api.get_sys_note_detail = params => vm.$u.post('/personal/infromInfo',params)
	
	/* 获取系统协议*/
	vm.$u.api.get_sys_protocal = params => vm.$u.post('/personal/getAgr',params)
	
	

	/*+------------------------------------------------------------------+
	  |         脸谱管理 5api
	--+------------------------------------------------------------------+*/

	/* 用户创建or修改脸谱数据*/
	vm.$u.api.user_creat_or_modify_facebook = params => vm.$u.post('/members/createLostFase', params)

	/* 获取家庭关系列表*/
	vm.$u.api.get_relation_list = () => vm.$u.post('/members/getLabel')

	/* 获取脸谱列表*/
	vm.$u.api.get_user_facebook_list = params => vm.$u.post('/members/getLostFaseList', params)

	/* 查看脸谱详情*/
	vm.$u.api.get_the_detailof_facebook = params => vm.$u.post('/members/getLostFaseDetail', params)

	/* 用户删除一张脸谱*/
	vm.$u.api.user_delete_one_facebook = params => vm.$u.post('/members/deleteLostFase', params)

	/*+------------------------------------------------------------------+
	  |         寻人管理 9api
	--+------------------------------------------------------------------+*/

	/* 用户提交或者修改寻人信息*/
	vm.$u.api.user_submit_or_modify_looKfor_info = params => vm.$u.post('/members/creatLostPerson', params)

	/* 获取首页寻人列表*/
	vm.$u.api.get_firstpage_lookfor_list = params => vm.$u.post('/members/getLostPersonList', params)

	/* 快速拉取脸谱信息列表(只有关键数据)，用于发布寻人时快速填充*/
	vm.$u.api.drag_facebook_list = params => vm.$u.post('/members/getMyFaceList', params)

	/* 用户解除寻人事件*/
	vm.$u.api.user_cancel_lookfor_event = params => vm.$u.post('/members/removeLostPerson', params)

	/* 用户删除寻人信息*/
	vm.$u.api.user_delete_lookfor_info = params => vm.$u.post('/members/deleteLostPerson', params)

	/* 客服在审核中心，点击某条信息，进入详情页面查看*/
	vm.$u.api.KF_check_detail_info = params => vm.$u.post('/members/getPersonDetail', params)

	/* 用户修改信息(审核被退回)*/
	vm.$u.api.info_rejected_user_modify = params => vm.$u.post('/members/getPersonDetail', params)

	/* 获取详情 进行中状态*/
	vm.$u.api.get_looking_state_detail_info = params => vm.$u.post('/members/getPersonDetail', params)

	/* 获取详情 已找回状态*/
	vm.$u.api.get_over_state_detail_info = params => vm.$u.post('/members/getPersonDetail', params)
	
	// 滑动地图查看信息
	vm.$u.api.get_userDistribution = params => vm.$u.post('/members/userDistribution', params)
	
	// 发布置顶留言
	vm.$u.api.send_top_comment = params => vm.$u.post('/members/setLeave', params)
	
	// 获取置顶留言
	vm.$u.api.get_top_comment = params => vm.$u.post('/members/getLeave', params)
	
	// 获取市区人数分布
	vm.$u.api.get_city_human_distribution = params => vm.$u.post('/members/getDistribute', params)
	
	
	

	/*+------------------------------------------------------------------+
	  |         客服管理 2api
	--+------------------------------------------------------------------+*/
	/*  客服审核寻人信息*/
	vm.$u.api.KF_check_detail = params => vm.$u.post('/operate/checkPerson', params)

	/*  客服获取审核列表*/
	vm.$u.api.KF_get_check_list = params => vm.$u.post('/operate/getCheckPersonList', params) 
	
	/*  客服获取认证列表*/
	vm.$u.api.KF_get_sgc_list = params => vm.$u.post('/operate/getCheckSgs', params) 
	
	/*  客服获取认证详情*/
	vm.$u.api.KF_get_sgc_detail = params => vm.$u.post('/operate/getSgsDetail', params) 
	
	/*  客服审核认证详情*/
	vm.$u.api.KF_sgc_detail = params => vm.$u.post('/operate/checkSgs', params) 
	
	/*  获取客服微信*/
	vm.$u.api.get_KF_QR = params => vm.$u.post('/personal/serviceInfo', params) 

	/*+------------------------------------------------------------------+
	  |         评论管理 4api
	--+------------------------------------------------------------------+*/
	/*  举报*/
	vm.$u.api.user_complain = params => vm.$u.post('/members/setTalkReport', params)

	/*  用户添加评论*/
	vm.$u.api.user_add_comments = params => vm.$u.post('/members/setTalk', params)

	/*  获取举报类型*/
	vm.$u.api.get_complain_types = params => vm.$u.post('/members/getTalkReport', params)

	/*  获取评论列表*/
	vm.$u.api.get_comments_list = params => vm.$u.post('/members/getTalkList', params)
	
	/*  获取会员评论点赞*/
	vm.$u.api.setlike = params => vm.$u.post('/members/setLike', params)
	
	/*  获取公益卡信息*/
	vm.$u.api.get_user_card_info = params => vm.$u.post('/members/getUserCard', params)
	
	
	

	/*+------------------------------------------------------------------+
	  |         公共接口 7api
	--+------------------------------------------------------------------+*/
	/*  添加手机号*/
	vm.$u.api.user_add_phone_number = params => vm.$u.post('/Personal/setMobile', params)

	/*  获取手机号码列表*/
	vm.$u.api.user_get_phone_list = () => vm.$u.post('/Personal/getMobileList')

	/*  人脸识别*/
	vm.$u.api.face_recognise = params => vm.$u.post('/Personal/faceCheck', params)

	/*  获取手机验证码*/
	vm.$u.api.get_phone_verify_code = params => vm.$u.post('/members/sendSms', params)

	/*  上传图片*/
	vm.$u.api.upload_face_img = params => vm.$u.post('/index/uploadImg', params)

	/*  获取虚拟号码*/
	vm.$u.api.get_virtual_phone_number = params => vm.$u.post('/Personal/getVirtual', params)

	/*  验证手机验证码*/
	vm.$u.api.verify_vericode = params => vm.$u.post('/members/verifySendSms', params)
	
	/*  上产base64照片*/
	vm.$u.api.upload_base64 = params => vm.$u.post('/index/uploadImgString', params)
	
	/*  获取省市区列表*/
	vm.$u.api.get_region_list = () => vm.$u.post('/index/areaList')
	
	/*  根据市id解析出中心点经纬度*/
	vm.$u.api.analyze_cityid_to_loc = params => vm.$u.post('/members/showLocation',params)
	
	

	/*+------------------------------------------------------------------+
	  |         分享管理
	--+------------------------------------------------------------------+*/
	/*  获取分享信息*/
	vm.$u.api.get_share_info = params => vm.$u.post('/personal/getShareContents', params)
	
	/*  获取分享信息票据*/
	vm.$u.api.get_share_ticket = params => vm.$u.post('/personal/getShareTicket', params)
	
	/*  获取分享海报*/
	vm.$u.api.get_share_picture = params => vm.$u.post('/personal/shareImg', params)
	
	/*+------------------------------------------------------------------+
	  |         下单管理
	--+------------------------------------------------------------------+*/
	/* 微信支付*/
	vm.$u.api.level_buy = () => vm.$u.post('/order/leverBuy')
	
	/*+------------------------------------------------------------------+
	  |         爱心圈管理
	--+------------------------------------------------------------------+*/
	/* 获取升级信息*/
	vm.$u.api.get_gradeInfo = () => vm.$u.post('/members/gradeInfo')
	
	/* 获取粉丝列表*/
	vm.$u.api.get_fans_list = params => vm.$u.post('/members/fansList',params)

	/*通过get的方式 将from_uid发送出去*/ 
	vm.$u.api.send_from_uid = params => vm.$u.get('/members/fansList',params)
	
	
}

export default {
	install
}
