const jweixin = require('jweixin-module')

export function configWeiXin(callback,url) {
	//懂点前端应该知道这里是查询接口的箭头函数，内部细节不写了，就是查询后端返回的公众号参数信息
	
	let apiList = [ // 可能需要用到的能力 需要啥就写啥。多写也没有坏处
		// 'updateAppMessageShareData',
		// 'updateTimelineShareData',
		// 'hideOptionMenu',
		// 'showOptionMenu',
		// 'chooseWXPay',
		'checkJsApi',
		// 'openLocation',
		// 'getLocation'
	];
	
	this.$u.api.get_share_ticket({
		current_url:url
	}).then(res=>{
		console.log("配置微信的结果",res)
		let info = {
			debug: true, // 调试，发布的时候改为false
			appId: result.data.appId,
			nonceStr: result.data.noncestr,
			timestamp: result.data.timestamp,
			signature: result.data.signature,
			jsApiList: apiList
		};
		jweixin.config(info);
		jweixin.error(err => {
			console.log('wx.js', err);
		});
		
		jweixin.ready(res => {
			if (callback) callback(jweixin);
		});
	}).catch()
}
